<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once '/home/www/spacekit_sub/1001/php/__pw.php';

require_once '/home/www/spacekit_sub/1001/module/sk_database/database.php';

require_once '/home/www/spacekit_sub/1001/php/library.php';

require_once '/home/www/spacekit_sub/1001/module/sk_xpath/xpath.php';

require_once '/home/www/spacekit_sub/1001/module/sk_dbedit/dbedit.php';


if (isset($_GET['letterboxdmovie'])) {
  letterboxdmovie();
  die;
}


function letterboxdmovie() {
  $entries = getMovieEntries();

  foreach ($entries as $entry) {
    $url = $entry['letterboxd_url'];

    $source_code = url_curl($url);
    $xpath = getXpath($source_code);

    $node = getLetterboxdMovieNode($xpath);

    $node['id'] = $entry['id'];
    $node['scraped'] = 1;
    updateTable('movie', $node);
  }
}


function getLetterboxdMovieNode($xpath) {
  $class = $xpath->query("//*[@class='text-link text-footer']");


  $runtime = $class[0]->nodeValue;
  $runtime = trim(explode('mins', $runtime)[0]);
  $runtime = str_replace("\xc2\xa0", '', $runtime);


  $imdb_url = $class[0]->getElementsByTagName('a')[0]->getAttribute('href');


  $imdb_id = '';

  $url_parts = explode('/title/', $imdb_url);

  if ($url_parts[1]) {
    $imdb_id = explode('/', $url_parts[1])[0];
  }


  $tmdb_url = $class[0]->getElementsByTagName('a')[1]->getAttribute('href');

  $tmdb_id = '';

  if (strpos($tmdb_url, '/tv/') !== false) {
    $url_parts = explode('/tv/', $tmdb_url);

    if (isset($url_parts[1])) {
      $tmdb_id = explode('/', $url_parts[1])[0];
    }
  }

  if (strpos($tmdb_url, '/movie/') !== false) {
    $url_parts = explode('/movie/', $tmdb_url);

    if (isset($url_parts[1])) {
      $tmdb_id = explode('/', $url_parts[1])[0];
    }
  }





  $class = $xpath->query("//*[@class='poster-list -p230 no-hover el col']");

  $poster_img_src = $class[0]->getElementsByTagName('img')[0]->getAttribute('src');



  $class = $xpath->query("//small[@class='number']");

  $year = trim($class[0]->nodeValue);




  $class = $xpath->query("//section[@class='film-header-lockup']");

  $title = $class[0]->getElementsByTagName('h1')[0]->nodeValue;
  $title = str_replace("\xc2\xa0", ' ', $title);
  $original_title = $title;

  $element = $class[0]->getElementsByTagName('em');
  if ($element->length) {
    $original_title = $element[0]->nodeValue;
    $original_title = str_replace(['‘','’'], '', $original_title);
  }


  $spans = $class[0]->getElementsByTagName('span');
  foreach ($spans as $v) {
    $director[] = trim($v->nodeValue);
  }


  $class = $xpath->query("//div[@class='backdrop-container']");

  $hero_img_src = '';
  if ($class->length) {
    $hero_img_src = $class[0]->getElementsByTagName('div')[0]->getAttribute('data-backdrop');
  }


  $class = $xpath->query("//div[@class='truncate']");
  $plot = trim($class[0]->nodeValue);
  $plot = str_replace("\xc2\xa0", ' ', $plot);



  $class = $xpath->query("//a[@class='text-slug tooltip']");

  $actor = array();
  $actor_json = array();

  foreach ($class as $v) {
    $name = trim($v->nodeValue);
    $letterboxd_url = trim($v->getAttribute('href'));
    $actor_json[] = array(
      'name' => $name,
      'letterboxd_url' => $letterboxd_url,
    );
    $actor[] = $name;
  }


  $node = array(
    'imdb_id' => $imdb_id,
    'imdb_url' => $imdb_url,
    'poster_img_src' => $poster_img_src,
    'year' => $year,
    'original_title' => $original_title,
    'director' => implode(', ', $director),
    'hero_img_src' => $hero_img_src,
    'runtime' => $runtime,
    'plot' => $plot,
    'tmdb_url' => $tmdb_url,
    'tmdb_id' => $tmdb_id,
    'actor' => implode(', ', $actor),
    'actor_json' => json_encode($actor_json)
  );

  return $node;
}


function getMovieEntries() {
  $sql = "SELECT * FROM movie WHERE scraped = '0' LIMIT 2;";

  $result = queryFetch($sql);

  if (empty($result)) {
    echo "STOP";
  }

  return $result;
}
