<?php

$page = 1;

if (!empty($_GET['letterboxdlist'])) {
  $page = $_GET['letterboxdlist'];
}

$url = 'https://letterboxd.com/peterstanley/list/1001-movies-you-must-see-before-you-die/by/release-earliest/page/' . $page . '/';


$source_code = url_curl($url);
$xpath = getXpath($source_code);

$nodes = getNodes($xpath);

$nodes = escapeStrings($nodes);

$sql = '';

foreach ($nodes as $v) {
  $value_table = array(
    'title' => $v['title'],
    'letterboxd_id' => $v['letterboxd_id'],
    'letterboxd_url' => $v['letterboxd_url'],
  );
  $sql .= getInsertStmt('movie', $value_table);
}

if (!$mysqli->multi_query($sql)) {
  echo "Multi query failed: (" . $mysqli->errno . ") " . $mysqli->error;
  die;
}

do {
  if ($res = $mysqli->store_result()) {
    $res->free();
  }
} while ($mysqli->more_results() && $mysqli->next_result());

die;


function getNodes($xpath) {
  $nodes = array();

  foreach ($xpath->query("//*[@class='poster-container']") as $key => $a) {
    $node = [];
    $image = $xpath->query("//img[@class='image']")->item($key);
    $div = $xpath->query("//div[@class='poster film-poster really-lazy-load']")->item($key);

    $node['title'] = $image->getAttribute('alt');
    $node['letterboxd_id'] = $div->getAttribute('data-film-id');
    $node['letterboxd_url'] = 'https://letterboxd.com' . $div->getAttribute('data-target-link');

    $nodes[] = $node;
  }

  return $nodes;
}