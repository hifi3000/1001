<?php

require_once './module/sk_error/error.php';

session_start();

$title = '1001 Movies';
$main_cookie = '1001';

require_once './php/__pw.php';

require_once './module/sk_database/database.php';

require_once './module/sk_login/login.php';

require_once './php/_cookie.php';

require_once './php/library.php';

require_once './module/sk_spotify/spotify.php';

require_once './module/sk_xpath/xpath.php';

require_once './module/sk_dbedit/dbedit.php';

if (isset($_GET['letterboxdlist'])) {
  require_once './php/letterboxd.php';
}

if (isset($_GET['letterboxdmovie'])) {
  letterboxdmovie();
  die;
}

function letterboxdmovie() {

  $entries = getMovieEntries();

  foreach ($entries as $entry) {
    $url = $entry['letterboxd_url'];

    $source_code = url_curl($url);
    $xpath = getXpath($source_code);

    $node = getLetterboxdMovieNode($xpath);

    $node['id'] = $entry['id'];
    $node['scraped'] = 1;
    updateTable('movie', $node);
  }
  die;
}


function getLetterboxdMovieNode($xpath) {

  $class = $xpath->query("//*[@class='text-link text-footer']");


  $runtime = $class[0]->nodeValue;
  $runtime = trim(explode('mins', $runtime)[0]);
  $runtime = str_replace("\xc2\xa0", '', $runtime);


  $imdb_url = $class[0]->getElementsByTagName('a')[0]->getAttribute('href');


  $imdb_id = '';

  $url_parts = explode('/title/', $imdb_url);

  if ($url_parts[1]) {
    $imdb_id = explode('/', $url_parts[1])[0];
  }


  $tmdb_url = $class[0]->getElementsByTagName('a')[1]->getAttribute('href');

  $tmdb_id = '';

  $url_parts = explode('/movie/', $tmdb_url);

  if ($url_parts[1]) {
    $tmdb_id = explode('/', $url_parts[1])[0];
  }




  $class = $xpath->query("//*[@class='poster-list -p230 no-hover el col']");

  $poster_img_src = $class[0]->getElementsByTagName('img')[0]->getAttribute('src');



  $class = $xpath->query("//small[@class='number']");

  $year = trim($class[0]->nodeValue);




  $class = $xpath->query("//section[@class='film-header-lockup']");

  $title = $class[0]->getElementsByTagName('h1')[0]->nodeValue;
  $title = str_replace("\xc2\xa0", ' ', $title);
  $original_title = $title;

  $element = $class[0]->getElementsByTagName('em');
  if ($element->length) {
    $original_title = $element[0]->nodeValue;
    $original_title = str_replace(['‘','’'], '', $original_title);
  }


  $spans = $class[0]->getElementsByTagName('span');
  foreach ($spans as $v) {
    $director[] = trim($v->nodeValue);
  }


  $class = $xpath->query("//div[@class='backdrop-container']");

  $hero_img_src = $class[0]->getElementsByTagName('div')[0]->getAttribute('data-backdrop');



  $class = $xpath->query("//div[@class='truncate']");
  $plot = trim($class[0]->nodeValue);
  $plot = str_replace("\xc2\xa0", ' ', $plot);



  $class = $xpath->query("//a[@class='text-slug tooltip']");

  foreach ($class as $v) {
    $name = trim($v->nodeValue);
    $letterboxd_url = trim($v->getAttribute('href'));
    $actor_json[] = array(
      'name' => $name,
      'letterboxd_url' => $letterboxd_url,
    );
    $actor[] = $name;
  }


  $node = array(
    'imdb_id' => $imdb_id,
    'imdb_url' => $imdb_url,
    'poster_img_src' => $poster_img_src,
    'year' => $year,
    'original_title' => $original_title,
    'director' => implode(', ', $director),
    'hero_img_src' => $hero_img_src,
    'runtime' => $runtime,
    'plot' => $plot,
    'tmdb_url' => $tmdb_url,
    'tmdb_id' => $tmdb_id,
    'actor' => implode(', ', $actor),
    'actor_json' => json_encode($actor_json)
  );

  return $node;
}


function getMovieEntries() {
  $sql = "SELECT * FROM movie WHERE scraped = '0' LIMIT 2;";

  $result = queryFetch($sql);

  return $result;
}

function getMovieEntry() {
  $sql = "SELECT * FROM movie LIMIT 1;";

  $result = queryFetch($sql);

  return $result[0];
}


if (isset($_POST['updateEntry'])) {
  $post = $_POST;

  $table = $_POST['table'];
  unset($post['table']);

  unset($post['updateEntry']);

  updateTable($table, $post);

  header("Location: /");
}

?>

<!DOCTYPE HTML>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title><?php echo $title; ?></title>
  <link rel="icon" href="/media/favicon.png">
  <link rel="stylesheet" type="text/css" href="css/mystyle.css">
  <script src="/js/toggleMenu.js"></script>
</head>

<body>

<?php

require_once './module/sk_table/table.php';

require_once './module/sk_warning/warning.php';

require_once './module/sk_header/header.php';

require_once './module/sk_navbar/navbar.php';

?>

<div class="main">


<?php

if (isset($_GET['dbedit'])) {
  $result = queryIdTable($_GET['dbid'], $_GET['dbedit']);
  displayForm($result, 'updateEntry', $_GET['dbedit']);
  die;
}

include_once './php/default.php';

?>


</div>
</body>
</html>